+++
title = "Contact"
slug = "contact"
+++

### Email

Feel free to reach out to me at
[paride@legovini.net](mailto:paride@legovini.net) for general inquiries, at
[paride@debian.org](mailto:paride@debian.org) for my Debian work, and at
[paride@ubuntu.com](mailto:paride@ubuntu.com) for my Ubuntu work.

PGP keys for the `@legovini.net` and `@debian.org` addresses are available via
[WKD](https://gnupg.org/faq/wkd.html): just run e.g. `gpg --locate-keys
paride@legovini.net`. Many modern email clients will do this automatically.
when configuded to do PGP encryption. Keys are also available at
[keys.openpgp.org](https://keys.openpgp.org/).

### Collaborative Development Platforms

I'm [github.com/paride](https://github.com/paride/),
[gitlab.com/paride](https://gitlab.com/paride/), and
[launchpad.net/~paride](https://launchpad.net/~paride).

### IRC

I'm `paride` on [Libera.Chat](https://libera.chat/) and
[OFTC](https://www.oftc.net/), but when in doubt please prefer dropping me an
email.

### Found an issue on this site?

The source of this website is hosted on a [public git
repository](https://gitlab.com/paride/paride.legovini.net), please [file an
issue](https://gitlab.com/paride/paride.legovini.net/-/issues) there.
